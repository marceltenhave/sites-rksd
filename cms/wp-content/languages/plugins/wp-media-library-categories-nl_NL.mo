��          �      �       0     1  8   5  
   n     y     �  
   �     �     �     �     �  6   �  :   5  /  p  	   �  N   �     �  %        ,  
   E     P  	   i     s     �  6   �  :   �              	                             
               Add Adds the ability to use categories in the media library. Categories Category changes are saved. Get Premium Version Jeffrey-WP Media Library Categories Remove Remove all categories View all categories https://codecanyon.net/user/jeffrey-wp/?ref=jeffrey-wp https://wordpress.org/plugins/wp-media-library-categories/ PO-Revision-Date: 2016-10-19 11:53:49+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.3.0-alpha
Language: nl
Project-Id-Version: Plugins - Media Library Categories - Stable (latest release)
 Toevoegen Voegt de mogelijkheid toe om categorieën in de mediabibliotheek te gebruiken. Categorieën Categoriewijzigingen zijn opgeslagen. Opwaarderen naar Premium Jeffrey-WP Media Library Categories Verwijder Verwijder alle categorieën Bekijk alle categorieën https://codecanyon.net/user/jeffrey-wp/?ref=jeffrey-wp https://wordpress.org/plugins/wp-media-library-categories/ 